import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private http: HttpClient) { }

  getWeatherForecast(cityId: string): Observable<any> {
    return this.http.get(
      environment.baseUrl +
      'group?id=' + cityId +
      '&appid=' + environment.appId
    );
  }
  getWeeklyForecast(cityId: string): Observable<any> {
    return this.http.get(
      environment.baseUrl +
      'forecast?id=' + cityId +
      '&appid=' + environment.appId
    );
  }
  private handleError(error: any) {
    let errMsg: string;
    errMsg = error.message ? error.message : error.toString();
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
