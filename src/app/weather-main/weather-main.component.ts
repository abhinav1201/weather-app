import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { WeatherService } from '../service/weather.service';
import * as _ from 'lodash';
import * as moment from 'moment';


@Component({
  selector: 'app-weather-main',
  templateUrl: './weather-main.component.html',
  styleUrls: ['./weather-main.component.scss']
})
export class WeatherMainComponent implements OnInit {
  weatherForecastData: any;
  errorMessage: string;
  weeklyWeatherForecastData: any;
  constructor(private _weatherService: WeatherService) { }
  ngOnInit() {
    const listOfStates = '2643743,2968815,3067696,3176959,3648559';
    this._weatherService.getWeatherForecast(listOfStates).subscribe(data => {
      if (data && data.list && data.list.length > 0) {
        // this.weatherForecastData = data.list;
        this.setWeatherData(data.list);
      }
    },
    error => this.errorMessage = <any>error);
  }

  setWeatherData(data) {
    _.forEach(data, (item) => {
      const sunsetTime = new Date(item.sys.sunset * 1000);
      const sunriseTime = new Date(item.sys.sunrise * 1000);
    item.sunset_time = sunsetTime.toLocaleTimeString();
    item.sunrise_time = sunriseTime.toLocaleTimeString();
    const currentDate = new Date();
    item.isDay = (currentDate.getTime() < sunsetTime.getTime());
    item.temp_celcius = (item.main.temp - 273.15).toFixed(0);
    item.temp_min = (item.main.temp_min - 273.15).toFixed(0);
    item.temp_max = (item.main.temp_max - 273.15).toFixed(0);
    item.temp_feels_like = (item.main.feels_like - 273.15).toFixed(0);
    item.iconUrl = `http://openweathermap.org/img/wn/${item.weather[0]['icon']}@2x.png`;
    item.day = moment(item.dt, ['h:mm A']).format('dddd HH:MM A');
    item.weatherCondition = item.weather[0]['description'];
    item.state = 'collapsed';
  });
    this.weatherForecastData = data;
  }
}
