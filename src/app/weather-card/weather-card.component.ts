import { Component, OnInit, Input } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { WeatherService } from '../service/weather.service';
import * as _ from 'lodash';
import * as moment from 'moment';
@Component({
  selector: 'app-weather-card',
  templateUrl: './weather-card.component.html',
  styleUrls: ['./weather-card.component.scss'],
  animations: [
    trigger('bodyExpansion', [
      state('collapsed, void', style({ height: '0px', visibility: 'hidden' })),
      state('expanded', style({ height: '*', visibility: 'visible' })),
      transition('expanded <=> collapsed, void => collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ])
  ]
})
export class WeatherCardComponent implements OnInit {

  constructor(private _weatherService: WeatherService) { }
  @Input() item: any;
  @Input() weeklyWeatherForecastData: any;
  ngOnInit() {
  }

  toggle(item): void {
    item.state = item.state === 'collapsed' ? 'expanded' : 'collapsed';
    if (item.state === 'expanded') {
      this.getWeeklyWeatherInfo(item);
    }
  }

  getWeeklyWeatherInfo(item): void {
    this._weatherService.getWeeklyForecast(item.id).subscribe(response => {
      if (response && response['list'] && response['list'].length > 0) {
         const dataArr = _.filter(response['list'], data => {
            if (moment(data.dt_txt).format('hh a') === '09 am') {
              data.temp_celcius = (data.main.temp - 273.15).toFixed(0);
              data.temp_min = (data.main.temp_min - 273.15).toFixed(0);
              data.temp_max = (data.main.temp_max - 273.15).toFixed(0);
              data.weekName = moment(data.dt_txt).format('dddd');
              return data;
            }
          });
       this.weeklyWeatherForecastData = dataArr;
      }
    });
  }

}
